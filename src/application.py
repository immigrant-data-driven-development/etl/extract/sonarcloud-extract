from decouple import config
from kafka import KafkaConsumer, KafkaProducer
import concurrent.futures
from sonarcloudx1 import factories
from pprint import pprint
import json

consumer = KafkaConsumer(
    config('TOPIC'),
    group_id=config('GROUP_ID'),
    bootstrap_servers=config('SERVERS'),
    auto_offset_reset='earliest',  
    enable_auto_commit=True,  
    value_deserializer=lambda x: json.loads(x.decode('utf-8'))
    )

def publish (**kwargs):
    pprint ("topic {}".format(kwargs["topic"]))
    pprint ("-"*50)
    
    producer = KafkaProducer(bootstrap_servers=config('SERVERS'),value_serializer=lambda v: json.dumps(v).encode('utf-8'))
    producer.send(kwargs["topic"], kwargs["data"])
    producer.flush()

def extract_function(project,extract):
    for key, value in extract.items():
        print ("{} {}".format(project['name'], key))
        value.get_by_project_function(project["key"],function=publish, topic="application.{}.{}".format("sonarcloud",key), extra_data = None)       


try:
    for message in consumer:
        
        mensagem = message.value
        
        personal_access_token  = mensagem["secret"]
        organization = mensagem["organization_name"]
        

        project_factory = factories.ProjectFactory(personal_access_token=personal_access_token, organization=organization)
        projects = project_factory.get_all_function(today=False,function=publish, topic="application.{}.{}".format("sonarcloud","project"), extra_data = None)  
        
        extract = {
            
            "measure"               : factories.MeasureFactory(personal_access_token=personal_access_token, organization=organization),
            "componentstree"        : factories.ComponentsTreeFactory(personal_access_token=personal_access_token, organization=organization),
            "duplications"          : factories.DuplicationsFactory(personal_access_token=personal_access_token, organization=organization),
            "issues"                : factories.IssuesFactory(personal_access_token=personal_access_token, organization=organization),
            "metrics"               : factories.MetricsFactory(personal_access_token=personal_access_token, organization=organization),
            "projectanalyses"       : factories.ProjectAnalysesFactory(personal_access_token=personal_access_token, organization=organization),
            "projectbranches"       : factories.ProjectBranchesFactory(personal_access_token=personal_access_token, organization=organization),
            "projectlinks"          : factories.ProjectLinksFactory(personal_access_token=personal_access_token, organization=organization),
            "projectpullrequests"   : factories.ProjectPullRequestsFactory(personal_access_token=personal_access_token, organization=organization),
            "supportedprograminglanguages"  : factories.SupportedPorgramimgLanguagesFactory(personal_access_token=personal_access_token, organization=organization),

         
        }

        with concurrent.futures.ProcessPoolExecutor(max_workers=100) as executor:
            list(executor.map(extract_function, projects,  [extract]*len(projects)))


except KeyboardInterrupt:
    print ("Interrupção do teclado detectada. Encerrando o consumidor");
finally:
    consumer.close()